CREATE TABLE  user  (
   id           INT(11) NOT NULL AUTO_INCREMENT,
   designation  VARCHAR(100) DEFAULT NULL,
   email        VARCHAR(100) DEFAULT NULL,
   full_name     VARCHAR(100) DEFAULT NULL,
   password     VARCHAR(32) DEFAULT NULL,
   username     VARCHAR(100) DEFAULT NULL,
  CONSTRAINT pk_user PRIMARY KEY (id)
);

CREATE TABLE  item  (
   id             INT(11) NOT NULL AUTO_INCREMENT,
   addition_time  DATETIME DEFAULT NULL,
   name           VARCHAR(100) DEFAULT NULL,
   added_by       INT(11) DEFAULT NULL,
  CONSTRAINT pk_item PRIMARY KEY (id),
  FOREIGN KEY (added_by) REFERENCES user (id)
);

CREATE TABLE  meal  (
   id                 INT(11) NOT NULL AUTO_INCREMENT,
   day                VARCHAR(100) DEFAULT NULL,
   modification_time  DATETIME DEFAULT NULL,
   slot               VARCHAR(100) DEFAULT NULL,
   modified_by        INT(11) DEFAULT NULL,
  CONSTRAINT pk_meal PRIMARY KEY (id),
  FOREIGN KEY (modified_by) REFERENCES user (id)
);

CREATE TABLE  meal_item  (
   meal_id  INT(11) NOT NULL,
   item_id  INT(11) NOT NULL,
  FOREIGN KEY (meal_id) REFERENCES meal (id),
  FOREIGN KEY (item_id) REFERENCES item (id)
);

CREATE TABLE  review  (
   id       INT(11) NOT NULL AUTO_INCREMENT,
   comment  VARCHAR(100) DEFAULT NULL,
   rating   DOUBLE DEFAULT NULL,
   time     DATETIME DEFAULT NULL,
  CONSTRAINT pk_review PRIMARY KEY (id)
);

CREATE TABLE  meal_review  (
   meal_id    INT(11) DEFAULT NULL,
   review_id  INT(11) NOT NULL,
  CONSTRAINT pk_meal_review PRIMARY KEY (review_id),
  FOREIGN KEY (meal_id) REFERENCES meal (id)
);

CREATE TABLE  user_review  (
   user_id    INT(11) DEFAULT NULL,
   review_id  INT(11) NOT NULL,
  CONSTRAINT pk_user_review PRIMARY KEY (review_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
);