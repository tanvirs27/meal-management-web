<%--
  User: shahriar
  Date: 2/28/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="<c:url value="/"/>" class="site_title">Therap</a>
        </div>

        <div class="clearfix"></div>

        <br/>

        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li><a href="<c:url value="/private/ViewAllMeal"/>"> View Meals </a></li>
                    <li><a href="<c:url value="/private/AddItem"/>"> Add item To Meal </a></li>
                    <li><a href="<c:url value="/private/ShowAllReview"/>"> View Reviews </a></li>
                    <li><a href="<c:url value="/private/AddReview"/>"> Give Review </a></li>
                </ul>
            </div>
            <div class="menu_section">
                <h3>Account</h3>
                <ul class="nav side-menu">
                    <li><a href="<c:url value="/public/Logout"/>"> Logout </a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <ul class="nav navbar-nav navbar-right">
                <li><a class="navbar-brand" href="<c:url value="/"/>">Meal Planner</a></li>
            </ul>
        </nav>
    </div>
</div>
