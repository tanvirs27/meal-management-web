<%@ page import="net.therap.mealmanagement.domain.Day" %>
<%@ page import="net.therap.mealmanagement.domain.Slot" %>
<%--
  User: shahriar
  Date: 2/22/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <jsp:include page="header.jsp"/>
    <title>View Meals</title>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <jsp:include page="navbar.jsp"/>

        <div class="right_col" role="main">
            <div>
                <div class="page-title">
                    <div class="title_left">
                        <h3>View Meals</h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 pull-right">
                            <a href="<c:url value="/private/ViewAllMeal"/>"
                               class="btn btn-block btn-primary"> View All </a>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">

                                <form action="<c:url value="/private/ViewMealBySlot"/>"
                                      method="post" class="form-inline form-label-left">

                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-2">Day</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="day" class="form-control col-md-7 col-xs-12">

                                                <c:set var="dayValues" value="<%=Day.getConcernedDays()%>"/>

                                                <option value="" selected disabled hidden>Choose a day</option>
                                                <c:forEach var="day" items="${dayValues}">
                                                    <option value="<c:out value="${day}"/>">
                                                        <c:out value="${day}"/>
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-2">Slot</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="slot" class="form-control col-md-7 col-xs-12">
                                                <option value="" selected disabled hidden>Choose a slot</option>

                                                <c:set var="slotValues" value="<%=Slot.values()%>"/>

                                                <option value="" selected disabled hidden>Choose a slot</option>
                                                <c:forEach var="slot" items="${slotValues}">
                                                    <option value="<c:out value="${slot}"/>">
                                                        <c:out value="${slot}"/>
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-2 col-sm-2 col-xs-2 col-md-offset-2">
                                            <button type="submit" class="btn btn-success">Search</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <c:forEach var="meal" items="${requestScope.allMeals}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">

                                <div class="title_right">
                                    <div class="col-md-2 col-sm-2 col-xs-2 pull-right">
                                        <form method="post" action="<c:url value="/private/AddItem"/>">
                                            <input type="hidden" name="day" value="${meal.day}"/>
                                            <input type="hidden" name="slot" value="${meal.slot}"/>

                                            <button type="submit" class="btn btn-block btn-primary">Add Item</button>
                                        </form>
                                    </div>
                                </div>

                                <div class="x_title list-inline">
                                    <h4><c:out value="${meal.day} ${meal.slot}"/>
                                    </h4>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <p><c:out value="Last Modified By
                                        ${meal.modifiedBy} at ${meal.lastModificationTime}"/>
                                    </p>

                                    <table class="table table-striped projects">
                                        <thead>
                                        <tr>
                                            <th style="width: 20%">Item Name</th>
                                            <th style="width: 20%">Added By</th>
                                            <th style="width: 20%">Addition Time</th>
                                            <th style="width: 20%">Remove</th>
                                            <th style="width: 20%">Update</th>
                                        </tr>
                                        </thead>
                                        <tbody id="table_body">

                                        <c:forEach var="item" items="${meal.items}">
                                            <tr>
                                                <td><c:out value="${item.name}"/>
                                                </td>
                                                <td><c:out value="${item.addedBy}"/>
                                                </td>
                                                <td><c:out value="${item.additionTime}"/>
                                                </td>
                                                <td>
                                                    <form method="post" action="<c:url value="/private/DeleteItem"/>">
                                                        <input type="hidden" name="meal_id"
                                                               value="<c:out value="${meal.id}"/>"/>
                                                        <input type="hidden" name="item_id"
                                                               value="<c:out value="${item.id}"/>"/>
                                                        <button type="submit" class="btn btn-danger btn-xs">Remove
                                                        </button>
                                                    </form>
                                                </td>
                                                <td>
                                                    <form method="post" action="<c:url value="/private/UpdateItem"/>">
                                                        <input type="hidden" name="day"
                                                               value=${meal.day}>
                                                        <input type="hidden" name="slot" value=${meal.slot}>
                                                        <input type="hidden" name="previousItem"
                                                               value=${item.name}>

                                                        <button type="submit" class="btn btn-info btn-xs">Update
                                                        </button>
                                                    </form>

                                                </td>
                                            </tr>
                                        </c:forEach>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>

    </div>
</div>

</body>
</html>
