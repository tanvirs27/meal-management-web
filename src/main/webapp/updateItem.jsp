<%--
  User: shahriar
  Date: 2/22/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <jsp:include page="header.jsp"/>
    <title>Update Item</title>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <jsp:include page="navbar.jsp"/>

        <div class="right_col" role="main">
            <div>
                <div class="page-title">
                    <div class="title_left">
                        <h3>Update an Item of a Meal</h3>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">

                                <form action="<c:url value="/private/UpdateItem"/>" method="post"
                                      class="form-horizontal form-label-left">

                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Day</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="day" class="form-control col-md-7 col-xs-12"
                                                   value="${requestScope.day}">
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Slot</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="slot" class="form-control col-md-7 col-xs-12"
                                                   value="${requestScope.slot}">
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Old Item Name</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="previousItem"
                                                   class="form-control col-md-7 col-xs-12"
                                                   value="${requestScope.previousItem}">
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Updated Item
                                            Name</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="updatedItem"
                                                   class="form-control col-md-7 col-xs-12"
                                                   placeholder="Name of the updated item">
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <button type="submit" class="btn btn-success">Add</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
