<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="net.therap.mealmanagement.domain.Day" %>
<%@ page import="net.therap.mealmanagement.domain.Slot" %>
<%--
  User: shahriar
  Date: 2/22/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<html>

<head>
    <jsp:include page="header.jsp"/>
    <title>Add Review</title>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <jsp:include page="navbar.jsp"/>

        <div class="right_col" role="main">
            <div>
                <div class="page-title">
                    <div class="title_left">
                        <h3>Give a review to a Meal</h3>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">

                                <form action="<c:url value="/private/AddReview"/>" method="post"
                                      class="form-horizontal form-label-left">

                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Day</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="day" class="form-control col-md-7 col-xs-12">

                                                <c:set var="dayValues" value="<%=Day.getConcernedDays()%>"/>

                                                <option value="" selected disabled hidden>Choose a day</option>
                                                <c:forEach var="day" items="${dayValues}">
                                                    <option value="<c:out value="${day}"/>">
                                                        <c:out value="${day}"/>
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Slot</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="slot" class="form-control col-md-7 col-xs-12">
                                                <option value="" selected disabled hidden>Choose a slot</option>

                                                <c:set var="slotValues" value="<%=Slot.values()%>"/>

                                                <option value="" selected disabled hidden>Choose a slot</option>
                                                <c:forEach var="slot" items="${slotValues}">
                                                    <option value="<c:out value="${slot}"/>">
                                                        <c:out value="${slot}"/>
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Rating</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" name="rating" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Give a rating">
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Comment</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea name="comment" class="form-control col-md-7 col-xs-12"
                                                      placeholder="Give a comment">
                                            </textarea>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <button type="submit" class="btn btn-success">Add Review</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
