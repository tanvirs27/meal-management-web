<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="net.therap.mealmanagement.domain.Day" %>
<%@ page import="net.therap.mealmanagement.domain.Slot" %>
<%--
  User: shahriar
  Date: 2/22/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <jsp:include page="header.jsp"/>
    <title>Reviews</title>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <jsp:include page="navbar.jsp"/>

        <div class="right_col" role="main">
            <div>
                <div class="page-title">
                    <div class="title_left">
                        <h3>Reviews</h3>
                    </div>

                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 pull-right">
                            <a href="<c:url value="/private/ShowAllReview"/>" class="btn btn-block btn-primary"> View All</a>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>


                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">

                                <form action="<c:url value="/private/ShowReviewByUser"/>" method="post" class="form-inline form-label-left">

                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <label class="control-label col-md-6 col-sm-6 col-xs-12">Reviewer Name</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                                            <input type="text" name="reviewer" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Type a reviewer name">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-2 col-sm-2 col-xs-2 col-md-offset-2">
                                            <button type="submit" class="btn btn-success">Filter By Reviewer</button>
                                        </div>
                                    </div>

                                </form>


                                <form action="<c:url value="/private/ShowReviewByMeal"/>" method="post" class="form-inline form-label-left">

                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-2">Day</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="day" class="form-control col-md-7 col-xs-12">

                                                <c:set var="dayValues" value="<%=Day.getConcernedDays()%>"/>

                                                <option value="" selected disabled hidden>Choose a day</option>
                                                <c:forEach var="day" items="${dayValues}">
                                                    <option value="<c:out value="${day}"/>">
                                                        <c:out value="${day}"/>
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-2">Slot</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select name="slot" class="form-control col-md-7 col-xs-12">
                                                <option value="" selected disabled hidden>Choose a slot</option>

                                                <c:set var="slotValues" value="<%=Slot.values()%>"/>

                                                <option value="" selected disabled hidden>Choose a slot</option>
                                                <c:forEach var="slot" items="${slotValues}">
                                                    <option value="<c:out value="${slot}"/>">
                                                        <c:out value="${slot}"/>
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-2 col-sm-2 col-xs-2 col-md-offset-2">
                                            <button type="submit" class="btn btn-success">Filter By Meals</button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">

                            <div class="clearfix"></div>

                            <div class="x_content">

                                <table class="table table-striped projects">
                                    <thead>
                                    <tr>
                                        <th style="width: 20%">Meal</th>
                                        <th style="width: 20%">Reviewer</th>
                                        <th style="width: 20%">Time</th>
                                        <th style="width: 20%">Rating</th>
                                        <th style="width: 20%">Comment</th>
                                    </tr>
                                    </thead>
                                    <tbody id="table_body">

                                    <c:forEach var="review" items="${requestScope.allReviews}">
                                        <tr>

                                            <td> <c:out value="${review.meal.day} ${review.meal.slot}"/>
                                            </td>
                                            <td> <c:out value="${review.reviewer.username}"/>
                                            </td>
                                            <td> <c:out value="${review.time}"/>
                                            </td>
                                            <td> <c:out value="${review.rating}"/>
                                            </td>
                                            <td> <c:out value="${review.comment}"/>
                                            </td>

                                        </tr>
                                    </c:forEach>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
