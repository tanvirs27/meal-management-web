<%--
  User: shahriar
  Date: 2/28/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="<c:url value="/css/bootstrap.min.css"/>" rel="stylesheet">

<link href="<c:url value="/css/font-awesome.min.css"/>" rel="stylesheet">

<link href="<c:url value="/css/main.css"/>" rel="stylesheet">

<script src="<c:url value="/js/bootstrap.min.js"/>"></script>