<%--
  User: shahriar
  Date: 2/22/18
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <jsp:include page="header.jsp"/>
    <title>Signup</title>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="navbar-brand" href="./">Meal Planner</a></li>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="right_col" role="main">
            <div>
                <div class="page-title">
                    <div class="title_left">
                        <h3>Sign up to Meal Planner</h3>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">

                                <form action="<c:url value="/public/Signup"/>" method="post"
                                      class="form-horizontal form-label-left">

                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Full Name</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="fullname" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Enter your full name">
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Designation</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="designation"
                                                   class="form-control col-md-7 col-xs-12"
                                                   placeholder="Enter your designation">
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="email" name="email" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Enter your email">
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Username</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="username" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Enter your username">
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>

                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="password" name="password"
                                                   class="form-control col-md-7 col-xs-12"
                                                   placeholder="Enter your password">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <button type="submit" class="btn btn-success">Signup</button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
