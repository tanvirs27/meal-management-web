package net.therap.mealmanagement.filter;

import net.therap.mealmanagement.domain.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shahriar
 * @since 2/25/18
 */
public class LoggedInFilter implements Filter {

    private FilterConfig config;

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        String getFromSession = config.getInitParameter("getFromSession");

        if (request.getSession().getAttribute(getFromSession) == null) {
            String url = config.getInitParameter("redirectedUrl");

            response.sendRedirect(url);
            chain.doFilter(req, resp);
        } else {
            chain.doFilter(req, resp);
        }
    }

    public void init(FilterConfig config) throws ServletException {
        this.config = config;
    }

    public FilterConfig getConfig() {
        return config;
    }

    public void setConfig(FilterConfig config) {
        this.config = config;
    }
}
