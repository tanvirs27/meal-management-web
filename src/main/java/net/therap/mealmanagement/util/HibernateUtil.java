package net.therap.mealmanagement.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author shahriar
 * @since 2/14/18
 */
@WebListener
public class HibernateUtil implements ServletContextListener {

    private static final String PERSISTENCE_UNIT_NAME = "persistence";

    private EntityManagerFactory entityManagerFactory;
    private static EntityManager em;

    public static EntityManager getEm() {
        return em;
    }

    public void closeEntityManager() {

        if (em != null && em.isOpen()) {
            em.close();
        }

        if (entityManagerFactory != null && entityManagerFactory.isOpen()) {
            entityManagerFactory.close();
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {

        Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
        entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        em = entityManagerFactory.createEntityManager();
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        closeEntityManager();
    }
}