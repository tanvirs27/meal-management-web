package net.therap.mealmanagement.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/15/18
 */
@Entity
@Table(name = "user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "full_name")
    private String fullName;

    private String username;

    private String password;

    private String designation;

    private String email;

    @OneToMany(mappedBy = "reviewer", cascade = CascadeType.ALL)
    private List<Review> allReviews;

    public User() {
        allReviews = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Review> getAllReviews() {
        return allReviews;
    }

    public void setAllReviews(List<Review> allReviews) {
        this.allReviews = allReviews;
    }

    public String printFormatter() {
        return username;
    }

    public String toString() {
        return printFormatter();
    }
}
