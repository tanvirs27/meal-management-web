package net.therap.mealmanagement.controller;


import net.therap.mealmanagement.dao.MealDao;
import net.therap.mealmanagement.domain.Meal;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author shahriar
 * @since 2/20/18
 */
@WebServlet(name = "ViewAllMealServlet", urlPatterns = {"/private/ViewAllMeal"})
public class ViewAllMealServlet extends HttpServlet {

    MealDao mealDao;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        mealDao = new MealDao();

        request.setAttribute("allMeals", mealDao.getAll());

        RequestDispatcher view = request.getRequestDispatcher("../meal.jsp");

        view.forward(request, response);
    }
}