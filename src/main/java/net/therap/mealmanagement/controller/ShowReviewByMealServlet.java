package net.therap.mealmanagement.controller;

import net.therap.mealmanagement.dao.ReviewDao;
import net.therap.mealmanagement.domain.Review;
import net.therap.mealmanagement.domain.User;
import net.therap.mealmanagement.util.Util;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * @author shahriar
 * @since 2/20/18
 */
@WebServlet(name = "ShowReviewByMealServlet", urlPatterns = {"/private/ShowReviewByMeal"})
public class ShowReviewByMealServlet extends HttpServlet {

    ReviewDao reviewDao;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        reviewDao = new ReviewDao();

        String day = request.getParameter("day");
        String slot = request.getParameter("slot");

        User user = (User) request.getSession().getAttribute(Util.USER);

        request.setAttribute("allReviews", reviewDao.getReviewByMeal(day, slot, user));

        RequestDispatcher view = request.getRequestDispatcher("../review.jsp");

        view.forward(request, response);
    }
}