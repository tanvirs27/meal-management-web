package net.therap.mealmanagement.controller;


import net.therap.mealmanagement.dao.UserDao;
import net.therap.mealmanagement.util.Util;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author shahriar
 * @since 2/20/18
 */
@WebServlet(name = "LogoutServlet", urlPatterns = {"/public/Logout"})
public class LogoutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();

        session.removeAttribute(Util.USER);
        session.invalidate();

        response.sendRedirect("Login");
    }
}