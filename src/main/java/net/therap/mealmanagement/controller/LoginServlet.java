package net.therap.mealmanagement.controller;


import net.therap.mealmanagement.dao.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author shahriar
 * @since 2/20/18
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/public/Login"})
public class LoginServlet extends HttpServlet {

    UserDao userDao;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.sendRedirect("../login.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        userDao = new UserDao();
        HttpSession session = request.getSession();

        if (userDao.authenticateUser(username, password)) {
            session.setAttribute("user", userDao.getByUsername(username));
            response.sendRedirect("../private/ViewAllMeal");
        } else {
            response.sendRedirect("../login.jsp");
        }
    }
}