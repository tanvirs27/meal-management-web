package net.therap.mealmanagement.controller;


import net.therap.mealmanagement.dao.MealDao;
import net.therap.mealmanagement.domain.User;
import net.therap.mealmanagement.util.Util;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shahriar
 * @since 2/20/18
 */
@WebServlet(name = "DeleteItemServlet", urlPatterns = {"/private/DeleteItem"})
public class DeleteItemServlet extends HttpServlet {

    MealDao mealDao;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        mealDao = new MealDao();

        User user = (User) request.getSession().getAttribute(Util.USER);

        int mealId = Integer.parseInt(request.getParameter("meal_id"));
        int itemId = Integer.parseInt(request.getParameter("item_id"));

        mealDao.deleteItemFromMeal(mealId, itemId, user);
        response.sendRedirect("ViewAllMeal");
    }
}