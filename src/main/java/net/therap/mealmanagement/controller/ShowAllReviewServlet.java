package net.therap.mealmanagement.controller;

import net.therap.mealmanagement.dao.ReviewDao;
import net.therap.mealmanagement.domain.Review;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * @author shahriar
 * @since 2/20/18
 */
@WebServlet(name = "ShowAllReviewServlet", urlPatterns = {"/private/ShowAllReview"})
public class ShowAllReviewServlet extends HttpServlet {

    ReviewDao reviewDao;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        reviewDao = new ReviewDao();

        request.setAttribute("allReviews", reviewDao.getAll());

        RequestDispatcher view = request.getRequestDispatcher("../review.jsp");

        view.forward(request, response);
    }
}