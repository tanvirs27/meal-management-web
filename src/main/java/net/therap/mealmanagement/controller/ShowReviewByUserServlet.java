package net.therap.mealmanagement.controller;

import net.therap.mealmanagement.dao.ReviewDao;
import net.therap.mealmanagement.domain.Review;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * @author shahriar
 * @since 2/20/18
 */
@WebServlet(name = "ShowReviewByUserServlet", urlPatterns = {"/private/ShowReviewByUser"})
public class ShowReviewByUserServlet extends HttpServlet {

    ReviewDao reviewDao;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        reviewDao = new ReviewDao();

        String reviewer = request.getParameter("reviewer");

        request.setAttribute("allReviews", reviewDao.getReviewByUser(reviewer));

        RequestDispatcher view = request.getRequestDispatcher("../review.jsp");

        view.forward(request, response);
    }
}