package net.therap.mealmanagement.controller;

import net.therap.mealmanagement.dao.MealDao;
import net.therap.mealmanagement.dao.ReviewDao;
import net.therap.mealmanagement.domain.Meal;
import net.therap.mealmanagement.domain.User;
import net.therap.mealmanagement.util.Util;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shahriar
 * @since 2/20/18
 */
@WebServlet(name = "AddReviewServlet", urlPatterns = {"/private/AddReview"})
public class AddReviewServlet extends HttpServlet {

    MealDao mealDao;
    ReviewDao reviewDao;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher("../addReview.jsp");

        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        mealDao = new MealDao();
        reviewDao = new ReviewDao();

        User user = (User) request.getSession().getAttribute(Util.USER);

        double rating;

        String day = request.getParameter("day");
        String slot = request.getParameter("slot");
        String comment = request.getParameter("comment");

        try {
            rating = Double.parseDouble(request.getParameter("rating"));
        } catch (NumberFormatException e) {
            rating = 0;
        }

        Meal meal = mealDao.getMeal(day, slot, user);

        reviewDao.addReviewToMeal(meal, rating, comment, user);

        response.sendRedirect("ShowAllReview");
    }
}