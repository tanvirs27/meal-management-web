package net.therap.mealmanagement.controller;


import net.therap.mealmanagement.dao.UserDao;
import net.therap.mealmanagement.domain.User;
import net.therap.mealmanagement.util.Util;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/**
 * @author shahriar
 * @since 2/20/18
 */
@WebServlet(name = "SignupServlet", urlPatterns = {"/public/Signup"})
public class SignupServlet extends HttpServlet {

    UserDao userDao;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher("../signup.jsp");
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        User user = new User();

        user.setUsername(request.getParameter("username"));
        user.setFullName(request.getParameter("fullname"));
        user.setDesignation(request.getParameter("designation"));
        user.setEmail(request.getParameter("email"));

        String password = request.getParameter("password");

        userDao = new UserDao();
        HttpSession session = request.getSession();

        try {
            user.setPassword(Util.getHashedPassword(password));

            userDao.addUser(user);

            session.setAttribute(Util.USER, userDao.getByUsername(user.getUsername()));
            response.sendRedirect("../private/ViewAllMeal");

        } catch (NoSuchAlgorithmException e) {
            RequestDispatcher view = request.getRequestDispatcher("../signup.jsp");
            view.forward(request, response);
        }
    }
}