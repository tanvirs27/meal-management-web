package net.therap.mealmanagement.controller;


import net.therap.mealmanagement.dao.MealDao;
import net.therap.mealmanagement.domain.User;
import net.therap.mealmanagement.util.Util;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shahriar
 * @since 2/20/18
 */
@WebServlet(name = "UpdateItemServlet", urlPatterns = {"/private/UpdateItem"})
public class UpdateItemServlet extends HttpServlet {

    MealDao mealDao;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        mealDao = new MealDao();

        User user = (User) request.getSession().getAttribute(Util.USER);

        String day = request.getParameter("day");
        String slot = request.getParameter("slot");
        String previousName = request.getParameter("previousItem");
        String updatedName = request.getParameter("updatedItem");

        if (updatedName != null) {
            mealDao.updateItemInMeal(day, slot, previousName, updatedName, user);
            response.sendRedirect("ViewAllMeal");
        } else {
            request.setAttribute("day", day);
            request.setAttribute("slot", slot);
            request.setAttribute("previousItem", previousName);

            RequestDispatcher view = request.getRequestDispatcher("../updateItem.jsp");

            view.forward(request, response);
        }
    }
}