package net.therap.mealmanagement.controller;


import net.therap.mealmanagement.dao.MealDao;
import net.therap.mealmanagement.domain.User;
import net.therap.mealmanagement.util.Util;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shahriar
 * @since 2/20/18
 */
@WebServlet(name = "AddItemServlet", urlPatterns = {"/private/AddItem"})
public class AddItemServlet extends HttpServlet {

    private MealDao mealDao;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher("../addItem.jsp");

        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        mealDao = new MealDao();

        User user = (User) request.getSession().getAttribute(Util.USER);

        String day = request.getParameter("day");
        String slot = request.getParameter("slot");
        String name = request.getParameter("item");

        if (name != null) {
            mealDao.addItemToMeal(day, slot, name, user);
            response.sendRedirect("ViewAllMeal");
        } else {
            request.setAttribute("day", day);
            request.setAttribute("slot", slot);

            RequestDispatcher view = request.getRequestDispatcher("../addItem.jsp");

            view.forward(request, response);
        }
    }
}