package net.therap.mealmanagement.dao;

import net.therap.mealmanagement.domain.Item;
import net.therap.mealmanagement.domain.User;
import net.therap.mealmanagement.util.QueryParameter;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author shahriar
 * @since 2/10/18
 */
public class ItemDao extends GenericDao<Item> {

    public ItemDao() {
        super(Item.class);
    }

    public Item getItem(String name, User user) {

        List<QueryParameter> parameters = new ArrayList<>();
        parameters.add(new QueryParameter("name", name));

        List<Item> results = getByProperty(parameters);

        Item item;

        if (results.size() == 1) {
            item = results.get(0);
        } else {
            item = new Item();
            item.setName(name);
            item.setAddedBy(user);
            item.setAdditionTime(new Date(System.currentTimeMillis()));
        }

        return item;
    }
}
