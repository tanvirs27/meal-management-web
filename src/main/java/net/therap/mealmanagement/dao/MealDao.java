package net.therap.mealmanagement.dao;

import net.therap.mealmanagement.domain.*;
import net.therap.mealmanagement.util.QueryParameter;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author shahriar
 * @since 2/10/18
 */
public class MealDao extends GenericDao<Meal> {

    private ItemDao itemDao;

    public MealDao() {
        super(Meal.class);
        this.itemDao = new ItemDao();
    }

    public List<Meal> getAll() {

        List<Meal> allMeals = new ArrayList<>();

        for (Day day : Day.values()) {
            for (Slot slot : Slot.values()) {
                allMeals.addAll(getMealBySlot(day.name(), slot.name()));
            }
        }
        return allMeals;
    }

    public List<Meal> getMealBySlot(String day, String slot) {

        List<QueryParameter> parameters = new ArrayList<>();
        parameters.add(new QueryParameter("day", day));
        parameters.add(new QueryParameter("slot", slot));

        return getByProperty(parameters);
    }

    public void addItemToMeal(String day, String slot, String itemName, User user) {

        Meal meal = getMeal(day, slot, user);

        Item item = itemDao.getItem(itemName, user);

        List<Item> allItems = meal.getItems();
        allItems.add(item);
        meal.setModifiedBy(user);
        meal.setLastModificationTime(new Date(System.currentTimeMillis()));

        update(meal);
    }

    public void deleteItemFromMeal(int mealId, int itemId, User user) {

        Meal meal = getById(mealId);

        Item item = itemDao.getById(itemId);

        List<Item> allItems = meal.getItems();
        allItems.remove(item);
        meal.setModifiedBy(user);
        meal.setLastModificationTime(new Date(System.currentTimeMillis()));

        update(meal);
    }


    public void updateItemInMeal(String day, String slot, String changedFrom, String changedTo, User user) {

        Meal meal = getMeal(day, slot, user);

        Item previousItem = itemDao.getItem(changedFrom, user);
        Item presentItem = itemDao.getItem(changedTo, user);

        List<Item> allItems = meal.getItems();
        allItems.remove(previousItem);
        allItems.add(presentItem);
        meal.setModifiedBy(user);
        meal.setLastModificationTime(new Date(System.currentTimeMillis()));

        update(meal);
    }

    public Meal getMeal(String day, String slot, User user) {

        List<Meal> results = getMealBySlot(day, slot);

        Meal meal;

        if (results.size() > 0) {
            meal = results.get(0);
        } else {
            meal = new Meal();
            meal.setDay(day);
            meal.setSlot(slot);
            meal.setModifiedBy(user);
            meal.setLastModificationTime(new Date(System.currentTimeMillis()));
        }

        return meal;
    }
}