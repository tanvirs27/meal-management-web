package net.therap.mealmanagement.dao;

import net.therap.mealmanagement.domain.Meal;
import net.therap.mealmanagement.domain.Review;
import net.therap.mealmanagement.domain.User;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author shahriar
 * @since 2/18/18
 */
public class ReviewDao extends GenericDao<Review> {

    private MealDao mealDao;
    private UserDao userDao;

    public ReviewDao() {
        super(Review.class);
        this.mealDao = new MealDao();
        this.userDao = new UserDao();
    }

    public void addReviewToMeal(Meal meal, double rating, String comment, User user) {

        Review review = new Review();

        review.setComment(comment);
        review.setRating(rating);
        review.setMeal(meal);
        review.setReviewer(user);
        review.setTime(new Date(System.currentTimeMillis()));

        update(review);
    }

    public List<Review> getReviewByUser(String username) {

        User user = userDao.getByUsername(username);

        return (user != null) ? user.getAllReviews() : new ArrayList<>();
    }

    public List<Review> getReviewByMeal(String day, String slot, User user) {

        Meal meal = mealDao.getMeal(day, slot, user);

        return meal.getReviews();
    }
}
