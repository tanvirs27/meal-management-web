package net.therap.mealmanagement.dao;

import net.therap.mealmanagement.domain.User;
import net.therap.mealmanagement.util.Util;
import net.therap.mealmanagement.util.QueryParameter;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/15/18
 */
public class UserDao extends GenericDao<User> {

    public UserDao() {
        super(User.class);
    }

    public User getUser(String username, String password) {

        String hashedPassword;

        try {
            hashedPassword = Util.getHashedPassword(password);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }

        List<QueryParameter> parameters = new ArrayList<>();
        parameters.add(new QueryParameter("username", username));
        parameters.add(new QueryParameter("password", hashedPassword));

        List<User> results = getByProperty(parameters);

        return (results.size() == 1) ? results.get(0) : null;
    }

    public User getByUsername(String username) {

        List<QueryParameter> parameters = new ArrayList<>();
        parameters.add(new QueryParameter("username", username));

        List<User> results = getByProperty(parameters);

        return (results.size() > 0) ? results.get(0) : null;
    }

    public boolean authenticateUser(String username, String password) {

        return getUser(username, password) != null;
    }

    public void addUser(User user) {
        save(user);
    }
}
