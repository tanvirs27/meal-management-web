package net.therap.mealmanagement.dao;

import net.therap.mealmanagement.util.HibernateUtil;
import net.therap.mealmanagement.util.QueryParameter;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 2/18/18
 */
public class GenericDao<T> {

    private EntityManager em;
    private Class<T> typeParameterClass;

    public GenericDao(Class<T> typeParameterClass) {
        this.em = HibernateUtil.getEm();
        this.typeParameterClass = typeParameterClass;
    }

    public T getById(int id) {
        return em.find(typeParameterClass, id);
    }

    public List<T> getAll() {

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(typeParameterClass);
        Root<T> root = criteriaQuery.from(typeParameterClass);

        criteriaQuery.select(root);

        TypedQuery<T> query = em.createQuery(criteriaQuery);

        return query.getResultList();
    }

    public List<T> getByProperty(List<QueryParameter> parameters) {

        if (parameters == null || parameters.size() == 0){
            return new ArrayList<>();
        }

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(typeParameterClass);
        Root<T> root = criteriaQuery.from(typeParameterClass);

        criteriaQuery.select(root);

        QueryParameter firstParameter = parameters.get(0);
        Predicate finalPredicate = criteriaBuilder.equal(root.get(firstParameter.getName()), firstParameter.getValue());

        for (int i = 1; i < parameters.size(); i++) {

            QueryParameter parameter = parameters.get(i);
            Predicate presentPredicate = criteriaBuilder.equal(root.get(parameter.getName()), parameter.getValue());

            finalPredicate = criteriaBuilder.and(finalPredicate, presentPredicate);
        }

        criteriaQuery.where(finalPredicate);

        return em.createQuery(criteriaQuery).getResultList();
    }

    public void update(T object) {
        em.getTransaction().begin();

        em.merge(object);

        em.getTransaction().commit();
    }

    public void save(T object) {
        em.getTransaction().begin();

        em.persist(object);

        em.getTransaction().commit();
    }

    public void delete(T object) {
        em.getTransaction().begin();

        em.remove(object);

        em.getTransaction().commit();
    }
}
